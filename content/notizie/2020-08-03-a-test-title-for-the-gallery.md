---
layout: blog
title: A test title for the gallery
date: 2020-08-03T18:48:16.993Z
image: /images/uploads/logo.png
galleryImages:
  - /images/uploads/logo.png
  - /images/uploads/lumelogo.png
  - /images/uploads/screenshot-from-2020-06-04-10-48-56.png
---
Some test text under the gallery