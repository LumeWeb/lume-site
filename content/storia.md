---
title: "Storia"
date: 2020-07-01T12:15:53+02:00
draft: false
layout: storia
heroimage: /images/LumeLogoSquare.png
---

LUMe, Laboratorio Universitario Metropolitano è un collettivo di studenti dell'Università degli Studi di Milano e Milano-Bicocca; delle Civiche Scuole di Cinema, di Musica e di teatro; del Conservatorio Giuseppe Verdi; della Accademia di Belle Arti di Brera che da Aprile del 2015 occupa lo stabile di Vicolo Santa Caterina 3/5.
In quanto studenti, lavoratori, musicisti e artisti crediamo nel valore politico e culturale della cooperazione artistica e costruiamo a LUMe un progetto che ne valorizzi la genesi sociale.
LUMe si fonda sulla partecipazione e sostiene pratiche collaborative volte a stimolare spirito critico e crescita interpersonale. Il collettivo è mutevole, solidale e inclusivo, vi si rispettano le storie e le esperienze delle diverse soggettività partecipanti riconoscendosi tutti e tutte nei valori dell'antifascismo, antirazzismo e antissessismo.

I progetti sostenuti da LUMe sono totalmente autofinanziati, sono costruiti grazie all'impegno e alla volontà di chi partecipa e sono estranei ad una logica di profitto.
LUMe promuove giovani emergenti in quanto crede nell'autoaffermazione della personalità tanto nella politica quanto nell'arte. In quanto spazio universitario LUMe si propone come laboratorio di dibattito, critica e di costruzione di pratiche e progetti alternativi nei confronti dell'accesso all'industria culturale e della condizione degli studenti universitari milanesi.
